# kubernetes

A variety of kubernetes tools pre-built from AUR:
kind

# Usage
Add the following lines to `/etc/pacman.conf`:

    [aurbuilds-kubernetes]
    SigLevel = Never
    Server = https://aurbuilds.gitlab.io/kubernetes/$arch
